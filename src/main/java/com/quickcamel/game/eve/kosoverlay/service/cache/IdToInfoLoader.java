/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.cache;

import com.beimin.eveapi.parser.eve.CharacterInfoParser;
import com.beimin.eveapi.response.eve.CharacterInfoResponse;
import com.google.common.cache.CacheLoader;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loads character information from the EVE API (including employment history)
 *
 * @author Louis Burton
 */
public class IdToInfoLoader extends CacheLoader<Long, CharacterInfoResponse> {

    private static final Logger m_logger = LoggerFactory.getLogger(IdToInfoLoader.class);

    @Override
    public CharacterInfoResponse load(Long pilotId) throws LoadingException {
        m_logger.debug("Loading Character Information from EVE API for " + pilotId);
        String errorMsg = "Unable to load Character Information from EVE API for " + pilotId;
        try {
            CharacterInfoResponse response = new CharacterInfoParser().getResponse(pilotId);
            if (response.hasError() || response.getCharacterName() == null) {
                m_logger.warn(errorMsg);
                if (response.hasError() && response.getError() != null) {
                    m_logger.warn(response.getError().toString());
                }
            }
            else {
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("Character Information for pilotId " + pilotId + " : " + ReflectionToStringBuilder.toString(response));
                }
                return response;
            }
        }
        catch (Exception e) {
            m_logger.error(errorMsg, e);
        }
        throw new LoadingException(errorMsg);
    }
}
