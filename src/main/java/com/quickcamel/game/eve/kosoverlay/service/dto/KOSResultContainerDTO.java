/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

/**
 * Description: Unmarshals the data returned by the CVA KOS API
 *
 * @author Louis Burton
 */
@XmlRootElement
@XmlSeeAlso({KOSPilotResultDTO.class, KOSCorpResultDTO.class, KOSAllianceResultDTO.class})
public class KOSResultContainerDTO {

    private int m_total;
    private int m_code;
    private String m_message;
    private List<KOSResultDTO> m_results;

    public int getTotal() {
        return m_total;
    }

    public void setTotal(int total) {
        m_total = total;
    }

    public int getCode() {
        return m_code;
    }

    public void setCode(int code) {
        m_code = code;
    }

    public String getMessage() {
        return m_message;
    }

    public void setMessage(String message) {
        m_message = message;
    }

    public List<KOSResultDTO> getResults() {
        return m_results;
    }

    public void setResults(List<KOSResultDTO> results) {
        m_results = results;
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("total", m_total).
                append("code", m_code).
                append("message", m_message).
                append("results", m_results).
                toString();
    }
}
