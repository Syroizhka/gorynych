/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.configuration;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

/**
 * A ConfigManager that works off of a simple Properties file relative to the current working directory
 *
 * @author Louis Burton
 */
public class PropertiesConfigManager implements IConfigManager {

    private static final Logger m_logger = LoggerFactory.getLogger(PropertiesConfigManager.class);
    private static final String COMMENTS = "Configuration properties for the KOSOverlay";

    private File m_configFile = new File("config/configuration.properties");
    private Properties m_properties = new Properties();
    private boolean m_disabled = false;

    public PropertiesConfigManager() {
        try {
            if (!m_configFile.exists()) {
                m_logger.debug("Creating new config file");
                FileUtils.write(m_configFile, "# " + COMMENTS);
            }
            else {
                try (InputStream inputStream = FileUtils.openInputStream(m_configFile)) {
                    m_properties.load(inputStream);
                }
            }
        }
        catch (IOException e) {
            m_logger.error("Config properties unable to be written - disabling", e);
            m_disabled = true;
        }
    }

    @Override
    public String getValue(String key) {
        return m_properties.getProperty(key);
    }

    @Override
    public String getValue(String key, String defaultValue) {
        return m_properties.getProperty(key, defaultValue);
    }

    @Override
    public void setValue(String key, String value) {
        m_properties.setProperty(key, value);
        if (!m_disabled) {
            try (OutputStream outputStream = FileUtils.openOutputStream(m_configFile)) {
                m_properties.store(outputStream, COMMENTS);
            }
            catch (IOException e) {
                m_logger.error("Unable to save property: key=" + key + ", value=" + value, e);
            }
        }
    }
}
